import * as Types from './types';

const initialState = {
  historyShop: '',
  profile: '',
  isAuthenticated: false,
  mess: '',
  isFetching: false,
  isCheckout: false,
  dataOTP: '',
  checkTokenOTP: '',
  isShowUseLink: false,
  errMsgEditPass: '',
  errMsgSDT: '',
  dataImageHoaDon: null,
  editPass: '',
  isRefreshing: true,
  isStopLoadHistory: false,
  // b:{
  //   c:{

  //   }
  // }
};

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case Types.LOGIN_SUCCESS:
      return {
        ...state,
        profile: action.payload,
        historyShop: '',
        isAuthenticated: false,
        mess: '',
        isFetching: false,
        isCheckout: false,
        dataOTP: '',
        checkTokenOTP: '',
        isShowUseLink: false,
        errMsgEditPass: '',
        errMsgSDT: '',
        dataImageHoaDon: null,
        editPass: '',
      };
    // case Types.Changc:
    //   return {
    //     ...state, b:{...state.b,c:'4'}
    //   };
    case Types.HISTORY_SHOP_SUCCESS:
      console.log(action.payload);
      return {
        ...state,
        isRefreshing: false,
        // historyShop: [...initialState.historyShop, action.payload.data],
        historyShop: action.payload.data,
        isStopLoadHistory: action.payload.data.length < 10 ? true : false,
      };
    case Types.RECOVER_HISTORY_SUCCESS:
      console.log(action.payload);
      return {
        ...state,
        isRefreshing: false,
        historyShop: state.historyShop.concat(action.payload.data),
        // historyShop: action.payload.data,
        isStopLoadHistory: action.payload.data.length < 10 ? true : false,
      };
    case Types.ISREFRESH:
      console.log(action.payload);
      return {
        ...state,
        isRefreshing: false,
      };
    case Types.SEEN_OTP_SUCCESS:
      return {
        ...state,
        dataOTP: action.payload,
        errMsgSDT: '',
      };
    case Types.CHECK_OTP_TOKEN_SUCCESS:
      return {
        ...state,
        checkTokenOTP: action.payload,
        isShowUseLink: true,
      };
    case Types.EDIT_PASS_ERROR:
      return {
        ...state,
        errMsgEditPass: action.payload,
      };
    case Types.SEEN_OTP_ERROR:
      return {
        ...state,
        errMsgSDT: action.payload,
      };
    case Types.UP_IMG_SUCCESS:
      return {
        ...state,
        dataImageHoaDon: action.payload,
      };
    case Types.EDIT_PASS_SUCCESS:
      return {
        ...state,
        editPass: action.payload,
        errMsgEditPass: '',
      };

    default:
      return state;
  }
};

export default userReducer;

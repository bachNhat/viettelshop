import * as Types from './types';

export const loginUser = data => {
  return {
    type: Types.LOGIN,
    payload: data,
  };
};

export const refreshToken = payload => {
  return {
    type: Types.REFRESH_TOKEN,
    payload,
  };
};

export const historyShop = data => {
  return {
    type: Types.HISTORY_SHOP,
    payload: data,
  };
};

export const editPass = data => {
  return {
    type: Types.EDIT_PASS,
    payload: data,
  };
};

export const senOTP = data => {
  return {
    type: Types.SEEN_OTP,
    payload: data,
  };
};

export const checkOTP = data => {
  return {
    type: Types.CHECK_OTP,
    payload: data,
  };
};

export const upImage = data => {
  return {
    type: Types.UP_IMG,
    payload: data,
  };
};

export const confirmHoaDon = data => {
  return {
    type: Types.CONFIRM_HOA_DON,
    payload: data,
  };
};

export const checkoutOrderCode = data => {
  return {
    type: Types.CHECK_ODER_CODE,
    payload: data,
  };
};

export const checkPhone = data => {
  return {
    type: Types.CHECK_PHONE,
    payload: data,
  };
};

export const listPromotion = data => {
  return {
    type: Types.LIST_PROMOTION,
    payload: data,
  };
};

export const reCoverHistory = data => {
  return {
    type: Types.RECOVER_HISTORY,
    payload: data,
  };
};


import {call, put, takeLatest, all} from 'redux-saga/effects';
import {
  login,
  history,
  setAuthToken,
  editpass_Get,
  sen_OTP,
  refreshToken,
  checkOTP_G,
  checkOtpToken,
  upImage,
  confrimHoa_Don,
  checkOutCode,
  check_Phone,
  get_List_Promotion,
} from '../../utils/api';
import * as Types from './types';
import storeT from '../../utils/store';
import {sleep} from '../../utils/utilities';
import {Alert} from 'react-native';
import helpers from '../../globals/helpers';
import {NavigationActions, StackActions} from 'react-navigation';

// import { showMessage, hideMessage } from "react-native-flash-message";
function* userLoginSaga(action) {
  console.log(action);
  try {
    helpers.showLoading();
    console.log(123);
    const loginUser = yield call(login, action.payload);

    const access_token = loginUser.token.token;
    console.log(access_token);
    yield put({
      type: Types.LOGIN_SUCCESS,
      payload: loginUser,
    });
    storeT.setToken(access_token);
    console.log(123);
    setAuthToken(access_token);

    // const hstr = yield call(history);
    // if (hstr.code == 401) {
    //   Alert.alert('Thông báo', hstr.msg);
    // }
    // yield put({
    //   type: Types.HISTORY_SHOP_SUCCESS,
    //   payload: hstr,
    // });
    console.log(111111);
    const resetAction = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({routeName: 'TabScreens'})],
    });
    action.payload.success.dispatch(resetAction);
    // action.payload.success.navigate('TabScreens');
    helpers.hideLoading();
  } catch (error) {
    helpers.showMessage('Nhập sai tài khoản !');
    // Alert.alert("Thông báo", "Nhập sai tài khoản !");
    console.log(error);
  }
}

export function* watchUserLogin() {
  yield takeLatest(Types.LOGIN, userLoginSaga);
}

function* historys(action) {
  console.log(action);
  try {
    const hstr = yield call(history, action.payload);
    // console.log(hstr);
    if (hstr.code == 401) {
      Alert.alert('Thông báo', hstr.msg);
    }
    yield put({
      type: Types.HISTORY_SHOP_SUCCESS,
      payload: hstr,
    });
  } catch (error) {
    yield put({
      type: Types.ISREFRESH,
    });
    // Alert.alert('Thông báo', 'Lỗi hệ thống!');
    console.log(error);
  }
}

export function* watchHistory() {
  yield takeLatest(Types.HISTORY_SHOP, historys);
}

function* reCoverHistory(action) {
  console.log(action);
  try {
    const hstr = yield call(history, action.payload);
    // console.log(hstr);
    if (hstr.code == 401) {
      Alert.alert('Thông báo', hstr.msg);
    }
    yield put({
      type: Types.RECOVER_HISTORY_SUCCESS,
      payload: hstr,
    });
  } catch (error) {
    yield put({
      type: Types.ISREFRESH,
    });
    // Alert.alert('Thông báo', 'Lỗi hệ thống!');
    console.log(error);
  }
}

export function* watchreCoverHistory() {
  yield takeLatest(Types.RECOVER_HISTORY, reCoverHistory);
}

function* refreshTokenUser(action) {
  console.log(action.payload.token, 'token');
  try {
    helpers.showLoading();
    yield setAuthToken(action.payload.token);
    const loginUser = yield call(refreshToken);
    yield put({
      type: Types.LOGIN_SUCCESS,
      payload: loginUser,
    });
    // const hstr = yield call(history);
    // if (hstr.code == 401) {
    //   Alert.alert('Thông báo', hstr.msg);
    // }

    // yield put({
    //   type: Types.HISTORY_SHOP_SUCCESS,
    //   payload: hstr,
    // });

    const resetAction = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({routeName: 'TabScreens'})],
    });
    action.payload.success.dispatch(resetAction);

    // action.payload.success.navigate('TabScreens');
    helpers.hideLoading();
  } catch (error) {
    // helpers.showMessage('Nhập sai tài khoản !')
    console.log(error);
  }
}

export function* watchrefreshTokenUser() {
  yield takeLatest(Types.REFRESH_TOKEN, refreshTokenUser);
}

function* editPass123(action) {
  console.log(action);
  try {
    const dataEdit = yield call(editpass_Get, action.payload);
    console.log(dataEdit);
    if (dataEdit.status == 400) {
      yield put({
        type: Types.EDIT_PASS_ERROR,
        payload: dataEdit.data.msg,
      });
    } else
      yield put({
        type: Types.EDIT_PASS_SUCCESS,
        payload: dataEdit,
      }),
        Alert.alert(
          'Thông báo',
          'Đổi mật khẩu thành công!',
          [
            {
              text: 'OK',
              style: 'cancel',
              onPress: () => {
                storeT.setToken('');
                action.payload.success.navigate('Login');
              },
            },
          ],
          {cancelable: false},
        );
  } catch (error) {
    Alert.alert('Thông báo', 'Lỗi hệ thống!');
    console.log(error);
  }
}

export function* watchEditPass() {
  yield takeLatest(Types.EDIT_PASS, editPass123);
}

function* senOTP(action) {
  // console.log(action)
  try {
    const dataSeen = yield call(sen_OTP, action.payload);
    console.log(dataSeen);
    if (dataSeen.status == 400) {
      yield put({
        type: Types.SEEN_OTP_ERROR,
        payload: dataSeen.data.msg,
      });
    } else {
      yield put({
        type: Types.SEEN_OTP_SUCCESS,
        payload: dataSeen,
      });
    }
  } catch (error) {
    Alert.alert('Thông báo', 'Lỗi hệ thống!');
    console.log(error);
  }
}

export function* watchSeenOTP() {
  yield takeLatest(Types.SEEN_OTP, senOTP);
}

function* checkOTP(action) {
  console.log(action);
  try {
    const dataCheckOTP = yield call(checkOTP_G, action.payload);
    console.log(dataCheckOTP);
    const tokenCheckOTP = dataCheckOTP.data.token;
    const checkOTPToken = yield call(checkOtpToken, tokenCheckOTP);
    console.log(checkOTPToken);
    yield put({
      type: Types.CHECK_OTP_TOKEN_SUCCESS,
      payload: checkOTPToken,
    });
    helpers.showMessage('Chúc mừng bạn đã gắn thẻ thành công');
  } catch (error) {
    Alert.alert('Thông báo', 'Lỗi hệ thống!');
    console.log(error.response);
  }
}

export function* watchCheckOTP() {
  yield takeLatest(Types.CHECK_OTP, checkOTP);
}

function* UpImage(action) {
  try {
    const upimg = yield call(upImage, action.payload);
    console.log(upimg);
    yield put({
      type: Types.UP_IMG_SUCCESS,
      payload: upimg,
    });
    // helpers.showMessage('Chúc mừng bạn đã gắn thẻ thành công')
  } catch (error) {
    Alert.alert('Thông báo', 'Lỗi hệ thống!');
    console.log(error.response);
  }
}

export function* watchUpImage() {
  yield takeLatest(Types.UP_IMG, UpImage);
}

function* confirmHoaDon(action) {
  console.log(action);
  try {
    helpers.showLoading();
    const confirmHoaDon = yield call(confrimHoa_Don, action.payload);
    helpers.hideLoading();
    console.log(confirmHoaDon);
    yield put({
      type: Types.CONFIRM_HOA_DON_SUCCESS,
      payload: confirmHoaDon,
    });
    helpers.showMessage(confirmHoaDon.msg);
  } catch (error) {
    // Alert.alert("Thông báo", "Lỗi hệ thống!");
    console.log(error.response);
  }
}

export function* watchConfrimHoaDon() {
  yield takeLatest(Types.CONFIRM_HOA_DON, confirmHoaDon);
}

function* checkOderCode(action) {
  console.log(action);
  try {
    const check_code = yield call(checkOutCode, action.payload);
    console.log(check_code);
    // yield put({
    //   type: Types.CONFIRM_HOA_DON_SUCCESS,
    //   payload: confirmHoaDon
    // });
    helpers.showMessage(check_code.msg);
  } catch (error) {
    // Alert.alert("Thông báo", "Lỗi hệ thống!");
    console.log(error.response);
  }
}

export function* watchCheckOderCode() {
  yield takeLatest(Types.CHECK_ODER_CODE, checkOderCode);
}

function* checkPhone(action) {
  console.log(action);
  try {
    const check_phone = yield call(check_Phone, action.payload);
    console.log(check_phone);
    // yield put({
    //   type: Types.CONFIRM_HOA_DON_SUCCESS,
    //   payload: confirmHoaDon
    // });
    helpers.showMessage(check_phone.msg);
  } catch (error) {
    // Alert.alert("Thông báo", "Lỗi hệ thống!");
    console.log(error.response);
  }
}

export function* watchCheckPhone() {
  yield takeLatest(Types.CHECK_PHONE, checkPhone);
}

function* listPromotion(action) {
  console.log(action);
  try {
    const list = yield call(get_List_Promotion, action.payload);
    console.log(list);
    // yield put({
    //   type: Types.CONFIRM_HOA_DON_SUCCESS,
    //   payload: confirmHoaDon
    // });
    helpers.showMessage(list.msg);
  } catch (error) {
    // Alert.alert("Thông báo", "Lỗi hệ thống!");
    console.log(error.response);
  }
}

export function* watchListPromotion() {
  yield takeLatest(Types.LIST_PROMOTION, listPromotion);
}

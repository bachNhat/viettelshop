import * as Types from './types';

const initialState = {
  infoShop: {},
  errInfoCode: '',
  isPopup: false,
};

const codeReducer = (state = initialState, action) => {
  switch (action.type) {
    case Types.GET_COCE_INFO_SUCCESS:
      return {
        ...state,
        infoShop: action.payload,
        errInfoCode: '',
        isPopup: true,
      };
    case Types.GET_COCE_INFO_ERROR:
      return {
        ...state,
        errInfoCode: action.payload,
        infoShop: {},
        isPopup: false,
      };
    default:
      return state;
  }
};

export default codeReducer;

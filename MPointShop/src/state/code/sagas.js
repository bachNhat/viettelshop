import {call, put, takeLatest, all} from 'redux-saga/effects';
import {
  login,
  history,
  setAuthToken,
  refreshToken,
  getCodeInfo,
} from '../../utils/api';
import * as Types from './types';
import helpers from '../../globals/helpers';

function* getInfoCode(action) {
  // console.log(action)
  try {
    helpers.showLoading();
    const codeInfo = yield call(getCodeInfo, action.payload);
    helpers.hideLoading();
    yield put({
      type: Types.GET_COCE_INFO_SUCCESS,
      payload: codeInfo,
    });
    console.log('code:::', codeInfo);
    if (codeInfo) {
      action.payload.success.navigate('DataCard');
    }
    // ;
  } catch (error) {
    console.log(error);
    yield put({
      type: Types.GET_COCE_INFO_ERROR,
      payload: error.response.data,
    });
    console.log(error.response);
    // helpers.showMessage(error)
  }
}

export function* watchgetInfoCode() {
  yield takeLatest(Types.GET_COCE_INFO, getInfoCode);
}

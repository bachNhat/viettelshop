import React, {Component} from 'react';
import {
  Text,
  View,
  ImageBackground,
  Dimensions,
  Image,
  TouchableOpacity,
  TextInput,
  ScrollView,
  KeyboardAvoidingView,
  BackAndroid,
  BackHandler,
  Alert,
  StatusBar,
} from 'react-native';
import {bindActionCreators} from 'redux';

import Wrapper from '../../components/wrapper';
import {codeActions} from '../../state/code';
import images from '../../constants/images';
import styles from './styles';
import {connect} from 'react-redux';
import {RNCamera} from 'react-native-camera';
import Ripple from 'react-native-material-ripple';
import {colors} from '../../constants/theme';
import {Icon} from 'native-base';
import helpers from '../../globals/helpers';
import SelectPopup from '../../components/popups/SelectPopup';
import KeyboardSpacer from 'react-native-keyboard-spacer';

const {width, height} = Dimensions.get('window');
class Identification extends Component {
  constructor(props) {
    super(props);
    this.state = {
      qr: '',
      popup: false,
    };
  }

  // this.props.dataQRCode.code == 0
  async checkOut() {
    this.props.getCodeInfo({
      code: this.state.qr,
      success: this.props.navigation,
    });
    this.setState({popup: true});
    // this.props.navigation.navigate('DataCard')
    // this.props.isPopup ? this.setState({ popup: true }) : null
  }

  render() {
    const {navigation} = this.props;
    return (
      <Wrapper flex={1} style={{backgroundColor: colors.GRAY2}}>
        <StatusBar backgroundColor={colors.PRIMARY} barStyle="light-content" />
        <ScrollView>
          <Wrapper style={styles.viewTop}>
            <Image
              style={{flex: 1}}
              source={require('../../assets/images/mPointShopv2_src_img_bgheader.jpg')}
            />
          </Wrapper>
          <Wrapper center middle>
            <Image
              style={{width: 120, height: 120, marginTop: -260}}
              source={images.LOGOLOGIN}
              resizeMode="contain"
            />
          </Wrapper>
          <Wrapper center middle>
            <TouchableOpacity
              onPress={() =>
                navigation.navigate('QRScan', {
                  checkOut: this.checkOut.bind(this),
                })
              }
              style={styles.touchScan}>
              <Wrapper center middle>
                <Icon name="qr-scanner" style={{color: colors.PRIMARY_DARK}} />
                <Text style={{color: colors.PRIMARY_DARK, fontWeight: '700'}}>
                  Quét mã
                </Text>
              </Wrapper>
            </TouchableOpacity>
            <Text style={styles.or}>Hoặc</Text>
            <Wrapper style={styles.viewInput} center middle>
              <Text style={styles.doiSoat}>Xác thực bằng mã</Text>
              <TextInput
                style={styles.textInput}
                onChangeText={text => this.setState({qr: text})}
                autoCapitalize="characters"
              />
              <Text style={{color: 'red', fontSize: 12}}>
                {this.props.errInfoCode.msg}
              </Text>
              <TouchableOpacity onPress={() => this.checkOut()}>
                <Wrapper center middle style={styles.touchSubmit}>
                  <Text style={styles.xacThuc}>Xác thực</Text>
                </Wrapper>
              </TouchableOpacity>
            </Wrapper>
          </Wrapper>
          <KeyboardSpacer />
        </ScrollView>
      </Wrapper>
    );
  }
}

const mapStateToProps = state => ({
  codeInfo: state.codeState.codeInfo,
  dataQRCode: state.codeState.infoShop,
  errInfoCode: state.codeState.errInfoCode,
  isPopup: state.codeState.isPopup,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getCodeInfo: codeActions.getcodeInfo,
    },
    dispatch,
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Identification);

import React, {Component} from 'react';
import {
  Text,
  View,
  ImageBackground,
  Dimensions,
  Image,
  TouchableOpacity,
  SafeAreaView,
  StatusBar,
} from 'react-native';
import Wrapper from '../../components/wrapper';
import images from '../../constants/images';
import styles from './styles';
const {width, height} = Dimensions.get('window');
import Icon from 'react-native-vector-icons/Ionicons';
import {RNCamera} from 'react-native-camera';
import Ripple from 'react-native-material-ripple';
import {colors} from '../../constants/theme';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {userActions} from '../../state/user';

class Extend extends Component {
  constructor(props) {
    super(props);
    this.state = {
      code: '',
    };
  }

  // componentWillUpdate() {
  //   console.log('hihihi');
  //   // this.props.listPromotion({code: this.state.code});
  // }
  // componentDidUpdate() {
  //   // this.props.listPromotion({code: 258481667});
  //   console.log('hahahh');
  // }

  render() {
    const {navigation} = this.props;
    console.log(this.state.code);
    return (
      <Wrapper flex={1} style={{padding: 10}}>
        <StatusBar backgroundColor={colors.PRIMARY} barStyle="light-content" />
        <Image
          source={images.LOGOLOGIN}
          style={styles.imageLogo}
          resizeMode="contain"
        />
        <Ripple
          // onPress={() =>
          //   navigation.navigate('QRScan', {
          //     back: true,
          //     qr: codeQR => this.setState({code: codeQR.data}),
          //   })
          // }
          onPress={() => alert('Chức năng đang trong quá trình nâng cấp')}>
          <Wrapper style={[styles.item, {backgroundColor: '#a67107'}]} row>
            <Wrapper center middle flex={1}>
              <Image
                source={images.ICONQRCODE}
                style={[styles.iconItem, {width: 50, height: 50}]}
                resizeMode="contain"
              />
            </Wrapper>
            <Wrapper flex={3} middle>
              <Text style={styles.titleItem}>Quét mã thẻ</Text>
              <Text style={styles.contentTextItem}>
                Quét mã thẻ để xem những ưu đãi có trong thẻ của bạn
              </Text>
            </Wrapper>
          </Wrapper>
        </Ripple>
        <Ripple
          // onPress={() => navigation.navigate('Invoice')}
          onPress={() => alert('Chức năng đang trong quá trình nâng cấp')}>
          <Wrapper style={[styles.item, {backgroundColor: '#a25d57'}]} row>
            <Wrapper center middle flex={1}>
              <Image
                source={images.ICONSMARTPHONE}
                style={[styles.iconItem, {width: 50, height: 50}]}
                resizeMode="contain"
              />
            </Wrapper>
            <Wrapper flex={3} middle>
              <Text style={styles.titleItem}>Xác thực mã hóa đơn</Text>
              <Text style={styles.contentTextItem}>
                Xác thực mã hóa đơn của bạn
              </Text>
            </Wrapper>
          </Wrapper>
        </Ripple>
        <Ripple
          // onPress={() => navigation.navigate('Phone')}
          onPress={() => alert('Chức năng đang trong quá trình nâng cấp')}>
          <Wrapper style={[styles.item, {backgroundColor: '#618F9E'}]} row>
            <Wrapper center middle flex={1}>
              <Image
                source={images.ICONPHONENUMBER}
                style={[styles.iconItem, {width: 50, height: 50}]}
                resizeMode="contain"
              />
            </Wrapper>
            <Wrapper flex={3} middle>
              <Text style={styles.titleItem}>Xác thực bằng số điện thoại</Text>
              <Text style={styles.contentTextItem}>
                Xác thực bằng số điện thoại của khách hàng
              </Text>
            </Wrapper>
          </Wrapper>
        </Ripple>
      </Wrapper>
    );
  }
}

const mapStateToProps = state => ({
  // history_shop: state.user.historyShop,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      listPromotion: userActions.listPromotion,
    },
    dispatch,
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Extend);

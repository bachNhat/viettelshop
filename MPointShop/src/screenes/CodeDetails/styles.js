import { StyleSheet, Dimensions } from 'react-native';
import { colors } from '../../constants/theme';
const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  container:{
    backgroundColor: 'rgba(20, 20, 20, 0.3)',
    padding: 10,
    borderRadius: 8
  },
  text:{
    fontSize:14,
    marginBottom:5,
    color:'white'
  }
});

export default styles;

import React, {Component, useState} from 'react';
import {
  Text,
  View,
  ImageBackground,
  Dimensions,
  TouchableOpacity,
  Image,
  TextInput,
  ScrollView,
  KeyboardAvoidingView,
  StatusBar,
} from 'react-native';
import images from '../../constants/images';
import Wrapper from '../../components/wrapper';
import styles from './styles';
import {colors} from '../../constants/theme';
import {Icon} from 'native-base';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {userActions} from '../../state/user';
import SelectPopup from '../../components/popups/SelectPopup';
import moment from 'moment';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import ImagePicker from 'react-native-image-crop-picker';

const {width, height} = Dimensions.get('window');
class CodeDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      img: null,
      data: '',
    };
  }
  componentWillMount() {
    this.setState({
      data: this.props.navigation.getParam('detail', ''),
    });
  }

  render() {
    console.log(this.state.data);
    const {profile} = this.props;
    return (
      <>
        <StatusBar backgroundColor={colors.PRIMARY} barStyle="light-content" />
        {this.state.data ? (
          <ImageBackground
            source={images.BGLOGIN}
            style={{width, height, padding: 10}}>
            <ScrollView>
              <Wrapper style={styles.container} center middle>
                <Image
                  style={{width: 0.5 * width, height: 200, borderRadius: 5}}
                  source={{
                    uri: this.state.data.partner.logo,
                  }}
                  resizeMode="contain"
                />
                <Wrapper style={{width: '100%', paddingTop: 20}}>
                  <Text
                    style={[styles.text, {fontSize: 20, fontWeight: '600'}]}>
                    {this.state.data.partner.name}
                  </Text>
                  <Text style={[styles.text, {fontWeight: '600'}]}>
                    {this.state.data.promotion.name}
                  </Text>
                  <Text style={styles.text}>
                    <Text style={{fontWeight: '600'}}>Mã xác thực: </Text>
                    {this.state.data.code}
                  </Text>
                  <Text style={styles.text}>
                    <Text style={{fontWeight: '600'}}>Người dùng: </Text>
                    {profile.user.name}
                  </Text>
                  <Text style={styles.text}>
                    <Text style={{fontWeight: '600'}}>Số điện thoại: </Text>
                    {profile.user.phone}
                  </Text>
                  <Text style={styles.text}>
                    <Text style={{fontWeight: '600'}}>Ngày lấy mã: </Text>
                    {moment(this.state.data.getCodeAt).format('DD/MM/YYYY')}
                  </Text>
                  <Text style={styles.text}>
                    <Text style={{fontWeight: '600'}}>Hết hạn: </Text>
                    {moment(this.state.data.endDate).format('DD/MM/YYYY')}
                  </Text>
                  <Text style={styles.text}>
                    <Text style={{fontWeight: '600'}}>Hoá đơn: </Text>
                    {this.state.data.receipt
                      ? this.state.data.receipt
                      : 'Không có'}
                  </Text>
                  <Text style={styles.text}>
                    <Text style={{fontWeight: '600'}}>Hình ảnh xác nhận: </Text>
                    {this.state.data.receiptImage ? null : 'Không có'}
                  </Text>
                  <Image
                    style={{width: 0.5 * width, height: 180, borderRadius: 5}}
                    source={{
                      uri: this.state.data.receiptImage,
                    }}
                    resizeMode="contain"
                  />
                </Wrapper>
              </Wrapper>
              <Wrapper style={{height: 100}} />
            </ScrollView>
          </ImageBackground>
        ) : null}
      </>
    );
  }
}
CodeDetails.navigationOptions = ({navigation}) => ({
  title: 'Chi tiết code',
  headerStyle: {
    backgroundColor: colors.PRIMARY_DARK,
  },
  headerTintColor: '#fff',
  headerTitleStyle: {
    fontWeight: 'bold',
  },
});

const mapStateToProps = state => ({
  profile: state.user.profile,
});

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CodeDetails);

import React, {Component, useState} from 'react';
import {
  Text,
  View,
  ImageBackground,
  Dimensions,
  TouchableOpacity,
  Image,
  TextInput,
  ScrollView,
  KeyboardAvoidingView,
} from 'react-native';
import {TextInputMask} from 'react-native-masked-text';
import images from '../../constants/images';
import Wrapper from '../../components/wrapper';
import styles from './styles';
import {colors} from '../../constants/theme';
import {Icon} from 'native-base';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {userActions} from '../../state/user';
import SelectPopup from '../../components/popups/SelectPopup';
import moment from 'moment';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import ImagePicker from 'react-native-image-crop-picker';

const {width, height} = Dimensions.get('window');
class DataCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      img: null,
      textHoaDon: '',
    };
  }

  openCamera() {
    ImagePicker.openCamera({
      width: 300,
      height: 400,
      // cropping: true,
    }).then(image => {
      console.log(image);
      this.setState({img: image});
      this.props.upImage({image});
    });
  }

  onSubmit() {
    // console.log(this.props.dataImageHoaDon);
    // console.log({
    //   receiptImage: this.props.dataImageHoaDon
    //     ? this.props.dataImageHoaDon.url
    //     : null,
    //   code: this.props.dataQRCode.promotionCode.code,
    //   // billAmount: this.props.dataQRCode.promotionCode.billAmount,
    //   billAmount: this.state.textHoaDon,
    //   receipt: null,
    //   success: this.props.navigation,
    // });
    // console.log(this.props.dataQRCode.promotionCode.billAmount);
    this.props.confirmHoaDon({
      receiptImage: this.props.dataImageHoaDon
        ? this.props.dataImageHoaDon.url
        : null,
      code: this.props.dataQRCode.promotionCode.code,
      billAmount: this.state.textHoaDon.replace(/\./g, ''),
      receipt: null,
      success: this.props.navigation,
    });
  }

  render() {
    // console.log(uri)
    // this.props.navigation.getParams('uri','');
    // this.props.navigation.state.params.uri;
    // const uri = this.props.navigation.getParams('uri', '');
    const {profile, checkTokenOTP, dataQRCode, navigation} = this.props;
    console.log('codeInfo');
    return (
      // <KeyboardAvoidingView style={{flex: 1}}>
      <Wrapper middle style={{paddingTop: 10, paddingHorizontal: 5}}>
        <ScrollView>
          <Image
            style={styles.imageData}
            source={{uri: this.props.dataQRCode.promotion.images[0]}}
            resizeMode="contain"
          />
          <Wrapper style={{padding: 10}}>
            <Text style={{marginVertical: 3, fontWeight: '500'}}>
              * Mã: {this.props.dataQRCode.promotionCode.code}
            </Text>
            <Text style={{marginVertical: 3, fontWeight: '500'}}>
              * Tên: {this.props.dataQRCode.promotion.name}
            </Text>
            <Text style={{marginVertical: 3, fontWeight: '500'}}>
              * Chi tiết: {'\n'}
              {this.props.dataQRCode.promotion.description}
            </Text>
            <Text style={{marginVertical: 3, fontWeight: '500'}}>
              * Ngày hết hạn:{' '}
              {moment(this.props.dataQRCode.promotion.endDate).format(
                'DD/MM/YYYY',
              )}
            </Text>

            {this.props.dataQRCode.promotionCode.status === 2 ? (
              <Wrapper>
                {this.props.dataQRCode.promotion.isBillPoint && (
                  <Wrapper>
                    <Text style={{fontSize: 15, fontWeight: '700'}}>
                      Tổng hoá đơn:
                    </Text>
                    <TextInputMask
                      type={'money'}
                      options={{
                        precision: 0,
                        separator: '.',
                        delimiter: '.',
                        unit: '',
                        suffixUnit: '',
                      }}
                      style={styles.input}
                      placeholder="Tổng hoá đơn"
                      value={this.state.textHoaDon}
                      onChangeText={text => this.setState({textHoaDon: text})}
                      keyboardType="numeric"
                    />
                  </Wrapper>
                )}
                <Wrapper>
                  <Text
                    style={{fontSize: 15, fontWeight: '700', marginTop: 10}}>
                    Chụp ảnh hoá đơn:
                  </Text>
                  {this.state.img ? null : (
                    <TouchableOpacity
                      style={styles.touchCamera}
                      onPress={() => this.openCamera()}>
                      <Wrapper center middle>
                        <Icon name="camera" style={{color: 'white'}} />
                      </Wrapper>
                    </TouchableOpacity>
                  )}
                  <Wrapper>
                    {this.state.img ? (
                      <Wrapper row>
                        <Wrapper flex={1}>
                          <Image
                            source={{uri: this.state.img.path}}
                            style={[styles.imgDefaul]}
                            resizeMode="cover"
                          />
                        </Wrapper>
                        <Wrapper
                          style={{marginLeft: 20}}
                          center
                          middle
                          flex={1}>
                          <TouchableOpacity
                            style={styles.touchHuy}
                            onPress={() => this.setState({img: ''})}>
                            <Text style={{color: 'white'}}>Huỷ</Text>
                          </TouchableOpacity>
                        </Wrapper>
                      </Wrapper>
                    ) : null}
                  </Wrapper>
                </Wrapper>
                <Wrapper center middle>
                  <TouchableOpacity
                    style={styles.touchXN}
                    onPress={() => this.onSubmit()}>
                    <Text style={{color: 'white'}}>Xác nhận</Text>
                  </TouchableOpacity>
                </Wrapper>
              </Wrapper>
            ) : (
              <Wrapper>
                <Text style={styles.notySD}>Mã ưu đãi đã được sử dụng</Text>
                <Text style={styles.notySD}>
                  Vào ngày:{' '}
                  {moment(this.props.dataQRCode.promotionCode.createdAt).format(
                    'DD/MM/YYYY',
                  )}
                </Text>
              </Wrapper>
            )}
          </Wrapper>
          <KeyboardSpacer />
        </ScrollView>
      </Wrapper>
      // </KeyboardAvoidingView>
    );
  }
}
DataCard.navigationOptions = ({navigation}) => ({
  title: 'Thông tin ưu đãi',
  headerStyle: {
    backgroundColor: colors.PRIMARY_DARK,
  },
  headerTintColor: '#fff',
  headerTitleStyle: {
    fontWeight: 'bold',
  },
});

const mapStateToProps = state => ({
  codeInfo: state.codeState.codeInfo,
  dataQRCode: state.codeState.infoShop,
  errInfoCode: state.codeState.errInfoCode,
  isPopup: state.codeState.isPopup,
  dataImageHoaDon: state.user.dataImageHoaDon,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      senOTP: userActions.senOTP,
      checkOTP: userActions.checkOTP,
      upImage: userActions.upImage,
      confirmHoaDon: userActions.confirmHoaDon,
    },
    dispatch,
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(DataCard);

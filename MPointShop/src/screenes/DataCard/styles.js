import {StyleSheet, Dimensions} from 'react-native';
import {colors} from '../../constants/theme';
const {width, height} = Dimensions.get('window');

const styles = StyleSheet.create({
  viewTop: {
    width,
    height: 220,
    backgroundColor: colors.PRIMARY_DARK,
    borderBottomLeftRadius: 100,
    borderBottomRightRadius: 100,
    overflow: 'hidden',
  },
  touchScan: {
    width: 130,
    height: 130,
    borderRadius: 100,
    backgroundColor: 'white',
    marginTop: -70,
  },
  or: {
    paddingVertical: 20,
    fontSize: 18,
  },
  viewInput: {
    width: 0.9 * width,
    height: 200,
    backgroundColor: 'white',
    borderRadius: 20,
  },
  touchSubmit: {
    paddingHorizontal: 100,
    paddingVertical: 10,
    backgroundColor: colors.PRIMARY_DARK,
    borderRadius: 10,
    marginTop: 20,
  },
  xacThuc: {
    color: 'white',
  },
  doiSoat: {
    color: colors.PRIMARY_DARK,
    fontSize: 16,
  },
  textInput: {
    width: '90%',
    height: 40,
    backgroundColor: colors.GRAY2,
    marginTop: 20,
    paddingLeft: 5,
  },
  inputXacNhan: {
    // width:'45%',
    height: 40,
    borderRadius: 5,
    borderColor: 'black',
    borderWidth: 1,
    paddingLeft: 10,
    flex: 1,
    marginRight: 10,
  },
  touchXN: {
    backgroundColor: colors.PRIMARY,
    width: '100%',
    paddingVertical: 10,
    paddingHorizontal: 30,
    borderRadius: 8,
    marginHorizontal: 10,
    marginTop: 20,
    marginBottom: 30,
    justifyContent: 'center',
    alignItems: 'center',
    // justifyContent:'space-between'
  },
  touchImage: {
    backgroundColor: colors.PRIMARY,
    borderRadius: 8,
    paddingHorizontal: 20,
    justifyContent: 'center',
    // paddingVertical: 10,
  },
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'black',
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  capture: {
    flex: 0,
    backgroundColor: '#fff',
    borderRadius: 5,
    padding: 15,
    paddingHorizontal: 20,
    alignSelf: 'center',
    margin: 20,
  },
  imgDefaul: {
    borderRadius: 8,
    width: '100%',
    height: 200,
    marginTop: 5,
  },
  imageData: {
    width: '100%',
    height: 200,
    // borderRadius: 8,
  },
  notySD: {
    fontSize: 18,
    color: colors.PRIMARY,
  },
  input: {
    width: 0.9 * width,
    height: 45,
    // backgroundColor: colors.GRAY,
    borderRadius: 8,
    marginTop: 6,
    paddingLeft: 10,
    borderColor: colors.GRAY,
    borderWidth: 1,
  },
  touchCamera: {
    width: 0.3 * width,
    height: 100,
    backgroundColor: colors.GRAY,
    marginTop: 5,
    borderRadius: 8,
    justifyContent: 'center',
    alignItems: 'center',
  },
  touchHuy: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 50,
    paddingVertical: 5,
    backgroundColor: colors.PRIMARY,
    borderRadius: 8,
  },
});

export default styles;

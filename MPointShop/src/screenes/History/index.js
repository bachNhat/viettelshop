import React, {Component} from 'react';
import {
  Text,
  View,
  FlatList,
  Image,
  Dimensions,
  ActivityIndicator,
  TouchableOpacity,
  StatusBar,
} from 'react-native';
import Wrapper from '../../components/wrapper';
import styles from './styles';
import {
  Container,
  Header,
  Content,
  Title,
  List,
  ListItem,
  Thumbnail,
  Left,
  Body,
  Right,
  Button,
  Icon,
} from 'native-base';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {userActions} from '../../state/user';
import {colors} from '../../constants/theme';
const {width, height} = Dimensions.get('window');
import PropTypes from 'prop-types';
import moment from 'moment';
const LIMIT = 10;
class History extends Component {
  constructor(props) {
    super(props);
    this.state = {
      refreshing: true,
      data: [],
    };
  }

  componentDidMount() {
    this.getData();
  }
  getData = () => {
    // this.setState({skip:0})
    try {
      const {history} = this.props;
      history({skip: 0, limit: LIMIT});
    } catch (error) {
      console.log('err get data history', error);
    }
  };

  pushData() {
    if (!this.props.isStopLoadHistory) {
      try {
        const {reCoverHistory} = this.props;
        reCoverHistory({skip: this.props.history_shop.length, limit: LIMIT});
      } catch (error) {
        console.log('err get data history', error);
      }
    }
  }

  render() {
    console.log(this.props.history_shop.data);
    const {navigation} = this.props;
    console.log('isRefreshing', this.props.isRefreshing);
    return (
      <Container>
        <Header style={{backgroundColor: colors.PRIMARY_DARK}}>
          <Left style={{flex: 1}} />
          <Body style={{flex: 5}}>
            <Title style={{color: 'white'}}>Lịch sử giao dịch</Title>
          </Body>
          <Right style={{flex: 1}} />
        </Header>
        <StatusBar backgroundColor={colors.PRIMARY} barStyle="light-content" />
        <FlatList
          ListEmptyComponent={
            this.props.isRefreshing == false ? (
              <Wrapper center middle>
                <Wrapper style={styles.viewErrList} center middle>
                  <Text style={{color: 'white'}}>
                    Không có lịch sử giao dịch
                  </Text>
                </Wrapper>
              </Wrapper>
            ) : null
          }
          ListFooterComponent={
            // this.props.history_shop ? (
            //   <Wrapper center middle>
            //     {/* <Wrapper style={styles.viewErrList} center middle> */}
            //     <Text style={{color: colors.PRIMARY}}>Cuối trang</Text>
            //     {/* </Wrapper> */}
            //   </Wrapper>
            // ) : null

            this.props.isStopLoadHistory ? (
              <Wrapper center middle>
                <Text style={{color: colors.PRIMARY}}>Cuối trang</Text>
              </Wrapper>
            ) : this.props.history_shop ? (
              <ActivityIndicator />
            ) : null
          }
          onEndThreshold={0}
          refreshing={this.props.isRefreshing}
          onRefresh={this.getData}
          onEndReached={() => this.pushData()}
          onEndReachedThreshold={0.3}
          keyExtractor={item => item.id + ''}
          data={this.props.history_shop}
          renderItem={({item, index}) => (
            <TouchableOpacity
              activeOpacity={0.8}
              onPress={() =>
                navigation.navigate('CodeDetails', {
                  detail: item,
                })
              }>
              <Wrapper style={{width, marginBottom: 10}} key={index}>
                <Wrapper center row>
                  <Image
                    style={{width: 100, height: 100}}
                    source={{
                      uri: item.partner.logo,
                    }}
                    resizeMode="contain"
                  />
                  <Wrapper style={styles.body} top flex={1}>
                    <Text style={{fontWeight: '700', marginBottom: 10}}>
                      {item.partner.name}
                    </Text>
                    <Text>{item.promotion.name}</Text>
                    <Text style={{marginVertical: 5}}>
                      Mã xác thực:{' '}
                      <Text style={{fontWeight: '600'}}>{item.code}</Text>
                    </Text>
                    <Wrapper style={{width: '100%'}} flex={1} bottom>
                      <Text style={{fontStyle: 'italic'}}>
                        Ngày đối soát:{' '}
                        {moment(item.checkoutAt).format('DD-MM-YYYY')}
                      </Text>
                    </Wrapper>
                  </Wrapper>
                </Wrapper>
              </Wrapper>
            </TouchableOpacity>
          )}
        />
      </Container>
    );
  }
}

// History.navigationOptions = ({ navigation }) => ({
//   title: 'Lịch sử đối soát',
//   headerStyle: {
//     backgroundColor: colors.PRIMARY_DARK,
//   },
//   headerTintColor: '#fff',
//   headerTitleStyle: {
//     fontWeight: 'bold',
//   },
// });

const mapStateToProps = state => ({
  history_shop: state.user.historyShop,
  isRefreshing: state.user.isRefreshing,
  isStopLoadHistory: state.user.isStopLoadHistory,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      history: userActions.historyShop,
      Refreshing: userActions.Refreshing,
      reCoverHistory: userActions.reCoverHistory,
    },
    dispatch,
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(History);

import {StyleSheet, Dimensions} from 'react-native';
import {colors} from '../../constants/theme';
const {width, height} = Dimensions.get('window');

const styles = StyleSheet.create({
  body: {
    marginLeft: 5,
    height: '80%',
    paddingHorizontal: 10,
    paddingVertical: 10,
    borderLeftColor: colors.PRIMARY_DARK,
    borderLeftWidth: 1,
    width: '90%',
  },
  viewErrList: {
    marginTop: 20,
    paddingHorizontal: 20,
    paddingVertical: 5,
    backgroundColor: colors.PRIMARY,
    borderRadius: 8,
  },
  textFooter: {
    marginTop: 20,
    color: colors.PRIMARY,
    marginBottom: 20,
  },
});

export default styles;

import React, {Component} from 'react';
import {View, StyleSheet, Text, TouchableOpacity} from 'react-native';
import Layout from '../../constants/layout';
import {colors} from '../../constants/theme';
import {RNCamera} from 'react-native-camera';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {userActions} from '../../state/user';
import {codeActions} from '../../state/code';

class ScanScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  reciveCode = false;
  onBarCodeRead = data => {
    // console.log(data)
    if (this.reciveCode) return;
    this.reciveCode = true;
    const backs = this.props.navigation.state.params.back;
    // const qr = this.props.navigation.state.params.qr;
    console.log(data);
    if (backs) {
      this.setState({isScan: false});
      // qr(data);
      this.props.navigation.goBack();

      this.props.navigation.state.params.onSelect({selected: data.data});
    } else {
      // qr(data);
      console.log('scan qrrrr');
      this.props.navigation.goBack();
      this.props.getCodeInfo({
        code: data.data,
        success: this.props.navigation,
      });
    }
    // this.reciveCode = false;
  };

  render() {
    return (
      <View style={styles.container}>
        <RNCamera
          ref={ref => {
            this.camera = ref;
          }}
          style={{flex: 1}}
          captureAudio={false}
          type={RNCamera.Constants.Type.back}
          flashMode={RNCamera.Constants.FlashMode.off}
          onBarCodeRead={this.onBarCodeRead}
          androidCameraPermissionOptions={{
            title: 'Permission to use camera',
            message: 'We need your permission to use your camera',
            buttonPositive: 'Ok',
            buttonNegative: 'Cancel',
          }}
          androidRecordAudioPermissionOptions={{
            title: 'Permission to use audio recording',
            message: 'We need your permission to use your audio',
            buttonPositive: 'Ok',
            buttonNegative: 'Cancel',
          }}>
          <View
            style={{
              width: '100%',
              backgroundColor: 'rgba(0,0,0,0.5)',
              height: Layout.window.height / 3,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text style={{color: '#fff', fontSize: 15}}>
              Di chuyển camera tới vùng chứa mã để quét{' '}
            </Text>
          </View>
          <View
            style={{
              width: '100%',
              height: Layout.window.height / 3,
              flexDirection: 'row',
            }}>
            <View
              style={{
                backgroundColor: 'rgba(0,0,0,0.5)',
                width: (Layout.window.width - Layout.window.height / 3) / 2,
              }}
            />
            <View
              style={{
                width: Layout.window.height / 3,
              }}
            />
            <View
              style={{
                backgroundColor: 'rgba(0,0,0,0.5)',
                width: (Layout.window.width - Layout.window.height / 3) / 2,
              }}
            />
          </View>
          <View
            style={{
              width: '100%',
              backgroundColor: 'rgba(0,0,0,0.5)',
              height: Layout.window.height / 3,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.pop();
              }}
              style={{
                backgroundColor: colors.PRIMARY,
                justifyContent: 'center',
                alignItems: 'center',
                width: Layout.window.width / 3,
                height: 40,
                borderRadius: 5,
              }}>
              <Text style={{color: '#fff'}}>Đóng</Text>
            </TouchableOpacity>
          </View>
        </RNCamera>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getCodeInfo: codeActions.getcodeInfo,
    },
    dispatch,
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ScanScreen);

import React, {Component, useState} from 'react';
import {
  Text,
  View,
  ImageBackground,
  Dimensions,
  TouchableOpacity,
  Image,
  TextInput,
} from 'react-native';
import images from '../../constants/images';
import Wrapper from '../../components/wrapper';
import styles from './styles';
import {colors} from '../../constants/theme';
import {Icon} from 'native-base';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {userActions} from '../../state/user';
import SelectPopup from '../../components/popups/SelectPopup';

const {width, height} = Dimensions.get('window');

class EditUserLink extends Component {
  constructor(props) {
    super(props);
    this.state = {
      SDT: '',
      OTPCheck: '',
      popup: false,
      card: false,
    };
  }

  componentDidMount() {
    const {profile} = this.props;
    profile ? this.setState({card: true}) : this.setState({card: false});
  }

  onSubmit() {
    const {checkOTP, dataOTP} = this.props;
    // console.log(dataOTP)
    if (this.state.OTPCheck) {
      checkOTP({id: dataOTP.otpId, otp: this.state.OTPCheck});
    }
    this.setState({popup: false});
  }

  onSeenOTP() {
    const {senOTP} = this.props;
    // if (this.props.dataOTP.code == 0) {
    //   this.setState({ popup: true })
    // } else (this.setState({ popup: false }))
    senOTP({phone: this.state.SDT});
  }
  componentDidUpdate(prevProps) {
    if (prevProps.dataOTP.code != this.props.dataOTP.code) {
      this.setState({popup: true});
    }
  }
  render() {
    const {profile, checkTokenOTP} = this.props;
    return (
      <Wrapper>
        <ImageBackground source={images.BGLOGIN} style={{width, height}}>
          <Wrapper flex={1} style={{padding: 10}}>
            {this.props.isShowUseLink || this.state.card ? (
              <Wrapper center middle>
                <Wrapper style={styles.card}>
                  <Wrapper center middle>
                    <Text
                      style={{
                        color: colors.PRIMARY_DARK,
                        fontWeight: '600',
                        fontSize: 18,
                      }}>
                      Tài khoản đã liên kết
                    </Text>
                  </Wrapper>
                  <Wrapper
                    style={{
                      width: '100%',
                      height: 1,
                      backgroundColor: colors.PRIMARY_DARK,
                      marginVertical: 10,
                    }}
                  />
                  <Text style={{fontSize: 15}}>Tên: {profile.user.name}</Text>
                  {profile.memberInfo ? (
                    <Text style={{fontSize: 15, marginTop: 10}}>
                      Email:
                      {profile.memberInfo.email ||
                        checkTokenOTP.memberInfo.email}
                    </Text>
                  ) : null}

                  <Text style={{fontSize: 15, marginTop: 10}}>
                    Phone: {profile.user.phone}
                  </Text>
                  <TouchableOpacity
                    onPress={() => this.setState({card: !this.state.card})}>
                    <Wrapper style={styles.touchSubmit} center middle>
                      <Text style={styles.textSubmit}>
                        Thay đổi tài khoản liên kết
                      </Text>
                    </Wrapper>
                  </TouchableOpacity>
                </Wrapper>
              </Wrapper>
            ) : (
              <Wrapper style={styles.content}>
                <Wrapper style={{paddingVertical: 10}}>
                  <Text>
                    Để thực hiện gắn mã thẻ tích điểm. Bạn cần xác thực số điện
                    thoại của 2 bước:
                  </Text>
                  <Text>1. Nhập số điện thoại để xác thực</Text>
                  <Text>2. Nhập mã xác thực otp nhận được trên điện thoại</Text>
                </Wrapper>
                <Wrapper row center style={styles.item}>
                  <Icon name="call" style={styles.icon} />
                  <TextInput
                    style={styles.textInvoice}
                    onChangeText={text => this.setState({SDT: text})}
                    // value={this.state.Invoice}
                    placeholder="Nhập số điện thoại"
                    placeholderTextColor={colors.GRAY}
                    keyboardType="number-pad"
                  />
                </Wrapper>
                {this.props.errMsgSDT ? (
                  <Text style={{color: 'red', fontSize: 12}}>
                    {this.props.errMsgSDT}
                  </Text>
                ) : null}
                <TouchableOpacity onPress={() => this.onSeenOTP()}>
                  <Wrapper style={styles.touchSubmit} center middle>
                    <Text style={styles.textSubmit}>Gửi mã xác nhận</Text>
                  </Wrapper>
                </TouchableOpacity>
              </Wrapper>
            )}

            <SelectPopup
              visible={this.state.popup}
              title={'Nhập mã OTP'}
              close={{
                onPress: () =>
                  this.setState({
                    popup: false,
                  }),
              }}
              card={
                <>
                  <Wrapper center middle>
                    <Text>Một mã OTP đã được gửi tới số điện thoại:</Text>
                    <Text>{this.state.SDT}</Text>
                    <TextInput
                      style={styles.input}
                      placeholder="Nhập mã OTP đễ xác nhận"
                      onChangeText={textCheckOTP => {
                        this.setState({OTPCheck: textCheckOTP});
                      }}
                      keyboardType="number-pad"
                    />
                    <TouchableOpacity onPress={() => this.onSubmit()}>
                      <Wrapper center middle style={styles.touchSub}>
                        <Text style={{color: 'white'}}>Xác nhận</Text>
                      </Wrapper>
                    </TouchableOpacity>
                  </Wrapper>
                </>
              }
            />
          </Wrapper>
        </ImageBackground>
      </Wrapper>
    );
  }
}

EditUserLink.navigationOptions = ({navigation}) => ({
  title: 'Thay đổi tài khoản liên kết',
  headerStyle: {
    backgroundColor: colors.PRIMARY_DARK,
  },
  headerTintColor: '#fff',
  headerTitleStyle: {
    fontWeight: 'bold',
  },
});

const mapStateToProps = state => ({
  profile: state.user.profile,
  dataOTP: state.user.dataOTP,
  checkTokenOTP: state.user.dataOTP,
  isShowUseLink: state.user.isShowUseLink,
  errMsgSDT: state.user.errMsgSDT,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      senOTP: userActions.senOTP,
      checkOTP: userActions.checkOTP,
    },
    dispatch,
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(EditUserLink);

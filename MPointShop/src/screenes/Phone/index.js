import React, {Component, useState} from 'react';
import {
  Text,
  View,
  ImageBackground,
  Dimensions,
  TouchableOpacity,
  Image,
  TextInput,
} from 'react-native';
import images from '../../constants/images';
import Wrapper from '../../components/wrapper';
import styles from './styles';
import {colors} from '../../constants/theme';
import {Icon} from 'native-base';
const {width, height} = Dimensions.get('window');
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {userActions} from '../../state/user';

const Phone = ({navigation, checkPhone}) => {
  const [codeBill, setCodeBill] = React.useState('');
  const [totalBill, setTotalBill] = React.useState('');
  const [phone, setPhone] = React.useState('');
  const [codeQR, setCodeQR] = useState('');
  const [selected, setSelected] = React.useState('');

  // onSelect = data => {
  //   setCodeInvoice(data.selected);
  // };

  const onSubmit = () => {
    console.log(phone);
    console.log(codeBill);
    console.log(totalBill);

    checkPhone({phone, totalBill, codeBill});
  };

  console.log(codeBill);

  return (
    <ImageBackground source={images.BGLOGIN} style={{width, height}}>
      <Text>{selected}</Text>
      <Wrapper flex={1} style={{padding: 10}}>
        <Wrapper style={styles.content}>
          <Wrapper row center style={styles.item}>
            <Icon name="call" style={styles.icon} />
            <TextInput
              style={styles.textInvoice}
              onChangeText={textPhone => setPhone(textPhone)}
              value={phone}
              placeholder="Nhập số điện thoại"
              placeholderTextColor={colors.GRAY}
              keyboardType="numeric"
            />
          </Wrapper>
          <Wrapper row center style={styles.item}>
            <Icon name="wallet" style={styles.icon} />
            <TextInput
              style={styles.textInvoice}
              onChangeText={textInvoice => setTotalBill(textInvoice)}
              value={totalBill}
              placeholder="Tổng hóa đơn (VND)(nếu có)"
              placeholderTextColor={colors.GRAY}
              keyboardType="numeric"
            />
          </Wrapper>
          <Wrapper row center style={[styles.item, {marginBottom: 0}]}>
            <Icon name="barcode" style={styles.icon} />
            <TextInput
              style={styles.textEndow}
              onChangeText={code => setCodeBill(code)}
              value={codeBill}
              placeholder="Mã hóa đơn (nếu có)"
              placeholderTextColor={colors.GRAY}
            />
            <Wrapper flex={1} center middle>
              <Text style={styles.or}>Hoặc</Text>
            </Wrapper>
            <TouchableOpacity
              onPress={() =>
                navigation.navigate('QRScan', {
                  back: true,
                  onSelect: this.onSelect,
                  qr: codeQR => setCodeBill(codeQR.data),
                })
              }>
              <Wrapper center middle style={styles.touchScan} flex={1}>
                <Text style={styles.textScan}>Quét QR</Text>
              </Wrapper>
            </TouchableOpacity>
          </Wrapper>
        </Wrapper>
        <TouchableOpacity onPress={onSubmit}>
          <Wrapper style={styles.touchSubmit} center middle>
            <Text style={styles.textSubmit}>Xác thực</Text>
          </Wrapper>
        </TouchableOpacity>
      </Wrapper>
    </ImageBackground>
  );
};

Phone.navigationOptions = ({navigation}) => ({
  title: 'Xác thực bằng số điện thoại',
  headerStyle: {
    backgroundColor: colors.PRIMARY_DARK,
  },
  headerTintColor: '#fff',
  headerTitleStyle: {
    fontWeight: 'bold',
  },
});

const mapStateToProps = state => ({
  history_shop: state.user.historyShop,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      checkPhone: userActions.checkPhone,
    },
    dispatch,
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Phone);

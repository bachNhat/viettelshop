import React, {Component, useState} from 'react';
import {
  Text,
  View,
  ImageBackground,
  Dimensions,
  TouchableOpacity,
  Image,
  TextInput,
  StatusBar,
} from 'react-native';
import images from '../../constants/images';
import Wrapper from '../../components/wrapper';
import styles from './styles';
import {colors} from '../../constants/theme';
import {Icon} from 'native-base';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {userActions} from '../../state/user';

const {width, height} = Dimensions.get('window');

const Invoice = ({navigation, checkoutOrderCode}) => {
  const [Invoice, setInvoice] = React.useState('');

  const submit = () => {
    checkoutOrderCode({code: Invoice});
  };
  return (
    <ImageBackground source={images.BGLOGIN} style={{width, height}}>
      <StatusBar backgroundColor={colors.PRIMARY} barStyle="light-content" />
      <Wrapper flex={1} style={{padding: 10}}>
        <Wrapper style={styles.content}>
          <Wrapper row center style={styles.item}>
            <Icon name="contact" style={styles.icon} />
            <TextInput
              style={styles.textEndow}
              onChangeText={textInvoice => setInvoice(textInvoice)}
              value={Invoice}
              placeholder="Nhập mã hóa đơn"
              placeholderTextColor={colors.GRAY}
            />
            <Wrapper flex={1} center middle>
              <Text style={styles.or}>Hoặc</Text>
            </Wrapper>
            <TouchableOpacity
              onPress={() =>
                navigation.navigate('QRScan', {
                  back: true,
                  qr: codeQR => setInvoice(codeQR.data),
                })
              }>
              <Wrapper center middle style={styles.touchScan} flex={1}>
                <Text style={styles.textScan}>Quét QR</Text>
              </Wrapper>
            </TouchableOpacity>
          </Wrapper>
        </Wrapper>
        <TouchableOpacity onPress={submit}>
          <Wrapper style={styles.touchSubmit} center middle>
            <Text style={styles.textSubmit}>Kiểm tra</Text>
          </Wrapper>
        </TouchableOpacity>
      </Wrapper>
    </ImageBackground>
  );
};

Invoice.navigationOptions = ({navigation}) => ({
  title: 'Xác thực mã hóa đơn',
  headerStyle: {
    backgroundColor: colors.PRIMARY_DARK,
  },
  headerTintColor: '#fff',
  headerTitleStyle: {
    fontWeight: 'bold',
  },
});

const mapStateToProps = state => ({
  history_shop: state.user.historyShop,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      checkoutOrderCode: userActions.checkoutOrderCode,
    },
    dispatch,
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Invoice);

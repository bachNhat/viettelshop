import React, {Component, useState} from 'react';
import {
  Text,
  View,
  ImageBackground,
  Dimensions,
  TouchableOpacity,
  Image,
  TextInput,
} from 'react-native';
import images from '../../constants/images';
import Wrapper from '../../components/wrapper';
import styles from './styles';
import {colors} from '../../constants/theme';
import {Icon} from 'native-base';
const {width, height} = Dimensions.get('window');

const Endow = () => {
  const [Endow, setEndow] = React.useState('');
  const [Invoice, setInvoice] = React.useState('');

  return (
    <ImageBackground source={images.BGLOGIN} style={{width, height}}>
      <Wrapper flex={1} style={{padding: 10}}>
        <Wrapper style={styles.content}>
          <Wrapper row center style={styles.item}>
            <Icon name="barcode" style={styles.icon}></Icon>
            <TextInput
              style={styles.textEndow}
              onChangeText={textEndow => setEndow(textEndow)}
              value={Endow}
              placeholder="Nhập mã ưu đãi"
              placeholderTextColor={colors.GRAY}
            />
            <Wrapper flex={1} center middle>
              <Text style={styles.or}>Hoặc</Text>
            </Wrapper>
            <TouchableOpacity>
              <Wrapper center middle style={styles.touchScan}>
                <Text style={styles.textScan}>Quét mã</Text>
              </Wrapper>
            </TouchableOpacity>
          </Wrapper>
          <Wrapper row center style={styles.item}>
            <Icon name="wallet" style={styles.icon}></Icon>
            <TextInput
              style={styles.textInvoice}
              onChangeText={textInvoice => setInvoice(textInvoice)}
              value={Invoice}
              placeholder="Tổng hóa đơn (VND)(nếu có)"
              placeholderTextColor={colors.GRAY}
            />
          </Wrapper>
          <Wrapper style={styles.line} />
          <Wrapper row style={[styles.item, {marginBottom: 0}]}>
            <Wrapper flex={1}>
              <Image
                source={images.IMGDEFAUL}
                style={[styles.imgDefaul]}
                resizeMode="cover"
              />
            </Wrapper>
            <Wrapper flex={1} style={{paddingLeft:10}}>
              <TouchableOpacity>
                <Wrapper center middle style={styles.touchPic}>
                  <Text style={[styles.textScan, {paddingHorizontal: 14}]}>
                    Chụp ảnh hóa đơn
                  </Text>
                </Wrapper>
              </TouchableOpacity>
            </Wrapper>
          </Wrapper>
        </Wrapper>
        <TouchableOpacity>
          <Wrapper style={styles.touchSubmit} center middle>
            <Text style={styles.textSubmit}>Xác thực</Text>
          </Wrapper>
        </TouchableOpacity>
      </Wrapper>
    </ImageBackground>
  );
};

export default Endow;

Endow.navigationOptions = ({navigation}) => ({
  title: 'Xác thực mã ưu đãi',
  headerStyle: {
    backgroundColor: colors.PRIMARY_DARK,
  },
  headerTintColor: '#fff',
  headerTitleStyle: {
    fontWeight: 'bold',
  },
});

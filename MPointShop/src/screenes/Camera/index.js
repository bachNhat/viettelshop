import React, { Component, useState } from 'react';
import {
  Text,
  View,
  ImageBackground,
  Dimensions,
  TouchableOpacity,
  Image,
  TextInput,
  ScrollView,
  KeyboardAvoidingView
} from 'react-native';
import images from '../../constants/images';
import Wrapper from '../../components/wrapper';
import styles from './styles';
import { colors } from '../../constants/theme';
import { Icon } from 'native-base';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { userActions } from "../../state/user";
import SelectPopup from '../../components/popups/SelectPopup';
import moment from 'moment';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import { RNCamera } from 'react-native-camera';


const { width, height } = Dimensions.get('window');
class Camera extends Component {

  constructor(props) {
    super(props);
    this.state = {
      image: ''
    }
  }

  takePicture = async () => {
    if (this.camera) {
      const options = { quality: 0.5, base64: true };
      const data = await this.camera.takePictureAsync(options);
      console.log(data.uri);
      this.setState({
        image: data.uri
      })
      // this.props.navigation.navigate('DataCard', {uri: data.uri})
    }
  };
  onSubmit(){
    this.props.upImage({ image: this.state.image})
  }

  render() {
    const { profile, checkTokenOTP, dataQRCode } = this.props
    return (
      <View style={styles.container}>
        {
          this.state.image ?
            <Wrapper flex={1} style={styles.wpImg}>
              <Image
                style={{ width, height: 0.7*height }}
                source={{ uri: this.state.image}}
                resizeMode='contain'
              />
              <Wrapper row center middle style={{ marginTop: 20}}>
                <TouchableOpacity onPress={() => this.setState({ image: ''})}>
                  <Wrapper center middle style={styles.touchSubmit} >
                    <Text style={{color:'white'}}>Chụp lại</Text>
                  </Wrapper>
                </TouchableOpacity>
                <TouchableOpacity onPress={()=>this.onSubmit()}>
                  <Wrapper center middle style={styles.touchSubmit}>
                    <Text style={{ color: 'white' }}>Xác nhận</Text>
                  </Wrapper>
                </TouchableOpacity>
              </Wrapper>
              
            </Wrapper>
            :
            <>
              <RNCamera
                ref={ref => {
                  this.camera = ref;
                }}
                style={styles.preview}
                type={RNCamera.Constants.Type.back}
                flashMode={RNCamera.Constants.FlashMode.on}
                androidCameraPermissionOptions={{
                  title: 'Permission to use camera',
                  message: 'We need your permission to use your camera',
                  buttonPositive: 'Ok',
                  buttonNegative: 'Cancel',
                }}
                androidRecordAudioPermissionOptions={{
                  title: 'Permission to use audio recording',
                  message: 'We need your permission to use your audio',
                  buttonPositive: 'Ok',
                  buttonNegative: 'Cancel',
                }}
                onGoogleVisionBarcodesDetected={({ barcodes }) => {
                  console.log(barcodes);
                }}
              />
              <View style={{ flex: 0, flexDirection: 'row', justifyContent: 'center' }}>
                <TouchableOpacity onPress={this.takePicture.bind(this)} style={styles.capture}>
                  <Text style={{ fontSize: 14 }}>Chụp</Text>
                </TouchableOpacity>
              </View>
            </>
        }

      </View>
    )
  }
}
Camera.navigationOptions = ({ navigation }) => ({
  title: 'Camera',
  headerStyle: {
    backgroundColor: colors.PRIMARY_DARK,
  },
  headerTintColor: '#fff',
  headerTitleStyle: {
    fontWeight: 'bold',
  },
});


const mapStateToProps = state => ({
  codeInfo: state.codeState.codeInfo,
  dataQRCode: state.codeState.infoShop,
  errInfoCode: state.codeState.errInfoCode,
  isPopup: state.codeState.isPopup,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      upImage: userActions.upImage,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Camera);

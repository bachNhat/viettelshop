import { StyleSheet, Dimensions } from 'react-native';
import { colors } from '../../constants/theme';
const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'black',
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  capture: {
    flex: 0,
    backgroundColor: '#fff',
    borderRadius: 5,
    padding: 15,
    paddingHorizontal: 20,
    alignSelf: 'center',
    margin: 20,
  },
  wpImg:{
    paddingTop:5,
    backgroundColor:'white'
  },
  touchSubmit:{
    paddingVertical: 5,
    paddingHorizontal:40,
    borderRadius: 5,
    backgroundColor:colors.PRIMARY,
    marginHorizontal:10,
    
  }
});

export default styles;

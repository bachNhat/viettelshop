import React, {Component} from 'react';
import {
  Text,
  View,
  ImageBackground,
  Dimensions,
  Image,
  TouchableOpacity,
  TextInput,
  KeyboardAvoidingView,
  ScrollView,
  Alert,
  StatusBar,
} from 'react-native';
import Wrapper from '../../components/wrapper';
import images from '../../constants/images';
import styles from './styles';
const {width, height} = Dimensions.get('window');
import {Content, List, ListItem, Right, Left, Icon} from 'native-base';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {userActions} from '../../state/user';
import SelectPopup from '../../components/popups/SelectPopup';
import {colors} from '../../constants/theme';
import storeT from '../../utils/store';
import helpers from '../../globals/helpers';
import KeyboardSpacer from 'react-native-keyboard-spacer';

class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      popup: false,
      mkCu: '',
      oldPassword: '',
      password: '',
      moiPassword: '',
      errMKMoi: false,
      errMKCu: false,
      submitRSPass: false,
    };
  }

  onLogOut() {
    storeT.setToken('');
    this.props.navigation.navigate('Login');
  }

  logOut() {
    Alert.alert(
      'Thông báo',
      'Bạn muốn đăng xuất khỏi tài khoản này ?',
      [
        {
          text: 'Hủy',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {text: 'Đồng ý', onPress: () => this.onLogOut()},
      ],
      {cancelable: false},
    );
  }

  onSubmit() {
    const {editPass} = this.props;
    editPass({
      oldPassword: this.state.oldPassword,
      newPassword: this.state.password,
      success: this.props.navigation,
    });
  }

  componentDidMount() {
    const getPassCu = storeT.getPassword().then(data => {
      this.setState({mkCu: data});
    });
    this.props.editpassSuccess ? this.setState({popup: false}) : null;
    console.log(this.props.isPopupEditMK);
  }

  // componentWillUpdate(){
  //   console.log('componentWillUpdate')
  // }

  // componentDidUpdate() {
  //   this.props.editpassSuccess ? this.setState({ popup: false}):null
  // }

  render() {
    const {navigation, profile} = this.props;

    return (
      <KeyboardAvoidingView style={{flex: 1}}>
        <StatusBar backgroundColor={colors.PRIMARY} barStyle="light-content" />
        <ImageBackground
          source={images.IMGHEADER}
          style={{
            width,
            height: 0.37 * height,
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Image
            source={images.USERIMG}
            style={styles.imageLogo}
            resizeMode="contain"
          />
          <Wrapper center middle style={styles.infomation}>
            <Text style={styles.name}>
              {profile.user.name} || ID: {profile.user.id}
            </Text>
            <Text style={styles.share}>
              {profile.user.email ? profile.user.email : 'Chưa liên kết email'}
            </Text>
            {profile.memberCard ? (
              <Text style={styles.poind}>{profile.memberCard.point} điểm</Text>
            ) : null}
          </Wrapper>
        </ImageBackground>
        <Wrapper style={styles.content}>
          <List>
            <ListItem onPress={() => this.setState({popup: true})}>
              <Left>
                <Wrapper row center>
                  <Icon name="key" />
                  <Text style={{marginLeft: 5}}>Đổi mật khẩu</Text>
                </Wrapper>
              </Left>
              <Right>
                <Icon name="arrow-forward" />
              </Right>
            </ListItem>
            <SelectPopup
              visible={this.state.popup}
              title={'Đổi mật khẩu'}
              close={{
                onPress: () =>
                  this.setState({
                    popup: false,
                    errMKMoi: false,
                    errMKCu: false,
                  }),
              }}
              card={
                <>
                  <Wrapper center middle>
                    <TextInput
                      style={styles.input}
                      placeholder="Nhập mật khẩu cũ (*)"
                      placeholderTextColor={'gray'}
                      onChangeText={mkcu => {
                        this.setState({oldPassword: mkcu});
                      }}
                    />
                    <TextInput
                      style={styles.input}
                      placeholder="Nhập mật khẩu mới (*)"
                      placeholderTextColor={'gray'}
                      onChangeText={pass => {
                        this.setState({password: pass});
                      }}
                    />
                    <TextInput
                      style={styles.input}
                      placeholder="Xác nhận lại mật khẩu (*)"
                      placeholderTextColor={'gray'}
                      onChangeText={mkMoi => {
                        this.setState({moiPassword: mkMoi});
                      }}
                    />
                    {this.props.errMsgEditPass ? (
                      <Text style={{color: 'red', fontSize: 12}}>
                        {this.props.errMsgEditPass}
                      </Text>
                    ) : null}
                    <TouchableOpacity onPress={() => this.onSubmit()}>
                      <Wrapper center middle style={styles.touchSub}>
                        <Text style={{color: 'white'}}>Thay đổi</Text>
                      </Wrapper>
                    </TouchableOpacity>
                  </Wrapper>
                </>
              }
            />
            <ListItem onPress={() => navigation.navigate('EditUserLink')}>
              <Left>
                <Wrapper row center>
                  <Icon name="share-alt" />
                  <Text style={{marginLeft: 5}}>Đổi tài khoản liên kết</Text>
                </Wrapper>
              </Left>
              <Right>
                <Icon name="arrow-forward" />
              </Right>
            </ListItem>
            <ListItem onPress={() => navigation.navigate('History')}>
              <Left>
                <Wrapper row center>
                  <Icon name="create" />
                  <Text style={{marginLeft: 5}}>Lịch sử xác thực</Text>
                </Wrapper>
              </Left>
              <Right>
                <Icon name="arrow-forward" />
              </Right>
            </ListItem>
            <ListItem onPress={() => this.logOut()}>
              <Left>
                <Wrapper row center>
                  <Icon name="log-out" />
                  <Text style={{marginLeft: 5}}>Đăng xuất</Text>
                </Wrapper>
              </Left>
              <Right>
                <Icon name="arrow-forward" />
              </Right>
            </ListItem>
          </List>
        </Wrapper>
      </KeyboardAvoidingView>
    );
  }
}

Profile.navigationOptions = ({navigation}) => ({
  header: null,
});

const mapStateToProps = state => ({
  profile: state.user.profile,
  errMsgEditPass: state.user.errMsgEditPass,
  editpassSuccess: state.user.editPass,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      editPass: userActions.editPass,
    },
    dispatch,
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Profile);

import {createDrawerNavigator} from "react-navigation-drawer";

import { createStackNavigator } from 'react-navigation-stack';
import Home from '../screenes/Home';

const Homes = createStackNavigator({ Home }, { defaultNavigationOptions: { header: null } })
//
import SideBar from '../components/sidebar';
//
const DrawerScreens = createDrawerNavigator(
  {
    Homes,
  },
  {
    initialRouteName: 'Homes',
    hideStatusBar: true,
    overlayColor: '#6b52ae',
    contentComponent: SideBar,
    defaultNavigationOptions: {
    header: null
    }
  },
)

export default DrawerScreens
import {createStackNavigator} from 'react-navigation-stack';
import StackScreens from './StackScreens';
import DrawerScreens from './DrawerScreens';
import TabScreens from './TabScreens';
import Endow from '../screenes/Endow';
import Invoice from '../screenes/Invoice';
import Phone from '../screenes/Phone';
import QRScan from '../screenes/QRScan';
import EditUserLink from '../screenes/EditUserLink';
import DataCard from '../screenes/DataCard';
import Camera from '../screenes/Camera';
import CodeDetails from '../screenes/CodeDetails';
import {StackActions, NavigationActions} from 'react-navigation';

// const resetAction = StackActions.reset({
//   index: 0,
//   actions: [NavigationActions.navigate({routeName: 'TabScreens'})],
// });

const AppNavigator = createStackNavigator(
  {
    StackScreens: {
      screen: StackScreens,
      navigationOptions: {
        header: null,
      },
    },
    TabScreens: {
      screen: TabScreens,
      navigationOptions: {
        header: null,
        gesturesEnabled: false,
      },
    },
    QRScan: {
      screen: QRScan,
      navigationOptions: {
        header: null,
      },
    },
    Endow,
    Invoice,
    Phone,
    EditUserLink,
    DataCard,
    Camera,
    CodeDetails,
  },
  {
    initialRouteName: 'StackScreens',
    // initialRouteName: 'TabScreens',
  },
);

export default AppNavigator;

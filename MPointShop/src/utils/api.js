import axios from 'axios';
import {baseUrl, endpoint, prefix} from '../constants/url';
import getUniqueString from './getUniqueString';
import storeT from '../utils/store';
import moment from 'moment';
import helpers from '../globals/helpers';

axios.defaults.baseURL = baseUrl;

export const setAuthToken = token => {
  // console.log(token)
  if (token) {
    axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
  } else {
    delete axios.defaults.headers.common['Authorization'];
  }
};

export async function login({username, password}) {
  try {
    const res = await axios.post(`/${endpoint.LOGIN}`, {
      username,
      password,
    });
    return res.data;
  } catch (error) {
    if (error.response) {
      helpers.showMessage(
        error.response.data.msg || 'Lỗi hệ thống.Vui lòng thử lại sau',
      );
    } else if (error.request) {
      console.log(error.request);
    } else {
      console.log('Error', error.message);
    }
    console.log(error.config);
  }
}

export async function getCodeInfo({code}) {
  try {
    const res = await axios.post(`/${endpoint.GET_CODE_INFO}`, {
      code,
    });
    return res.data;
  } catch (error) {
    if (error.response) {
      console.log(error.response);
      helpers.showMessage(
        error.response.data.msg || 'Lỗi hệ thống.Vui lòng thử lại sau',
      );
    } else if (error.request) {
      console.log(error.request);
    } else {
      console.log('Error', error.message);
    }
    console.log(error.config);
  }
}

export async function refreshToken() {
  try {
    const res = await axios.post(`/${endpoint.REFRESH_TOKEN}`, {});
    return res.data;
  } catch (error) {
    if (error.response) {
      helpers.showMessage(
        error.response.data.msg || 'Lỗi hệ thống.Vui lòng thử lại sau',
      );
    } else if (error.request) {
      console.log(error.request);
    } else {
      console.log('Error', error.message);
    }
    console.log(error.config);
  }
}

export async function history({skip, limit}) {
  console.log(skip, limit);
  try {
    const res = await axios.post(`/${endpoint.HISTORYSHOP}`, {
      skip,
      limit,
    });
    return res.data;
  } catch (error) {
    console.log(error);
    if (error.response) {
      // helpers.showMessage(
      //   error.response.data.msg || 'Lỗi hệ thống.Vui lòng thử lại sau',
      // );
    } else if (error.request) {
      console.log(error.request);
    } else {
      console.log('Error', error.message);
    }
    console.log(error.config);
  }
}

export async function editpass_Get({oldPassword, newPassword}) {
  try {
    const res = await axios.post(`/${endpoint.EDIT_PASS_URL}`, {
      oldPassword,
      newPassword,
    });
    console.log(res);
    console.log(res.data);
    return res.data;
  } catch (error) {
    if (error.response) {
      return error.response;
    } else if (error.request) {
      console.log(error.request);
    } else {
      console.log('Error', error.message);
    }
    console.log(error.config);
  }
}

export async function sen_OTP({phone}) {
  try {
    const res = await axios.post(`/${endpoint.SEEN_OTP}`, {
      phone,
    });
    console.log(res.data);
    return res.data;
  } catch (error) {
    if (error.response) {
      return error.response;
    } else if (error.request) {
      console.log(error.request);
    } else {
      console.log('Error', error.message);
    }
    console.log(error.config);
  }
}

export async function checkOTP_G({id, otp}) {
  try {
    const res = await axios.post(`/${endpoint.CHECK_OTP}`, {
      id,
      otp,
    });
    // console.log(res.data)
    return res.data;
  } catch (error) {
    if (error.response) {
      helpers.showMessage(
        error.response.data.msg || 'Lỗi hệ thống.Vui lòng thử lại sau',
      );
    } else if (error.request) {
      console.log(error.request);
    } else {
      console.log('Error', error.message);
    }
    console.log(error.config);
    // return Promise.reject(error.response);
  }
}

export async function checkOtpToken(otpToken) {
  console.log(otpToken);
  try {
    const res = await axios.post(`/${endpoint.CHECK_TOKEN_OTP}`, {
      otpToken,
    });
    return res.data;
  } catch (error) {
    if (error.response) {
      helpers.showMessage(
        error.response.data.msg || 'Lỗi hệ thống.Vui lòng thử lại sau',
      );
    } else if (error.request) {
      console.log(error.request);
    } else {
      console.log('Error', error.message);
    }
    console.log(error.config);
    // return Promise.reject(error.response);
  }
}

export async function upImage({image}) {
  let file = {
    uri: image.path,
    type: image.mime,
    name: 'file.jpg',
  };
  let data = new FormData();
  data.append('file', file);
  try {
    const res = await axios.post(`/${endpoint.UP_IMG}`, data, {
      headers: {
        'Content-Type': 'multipart/form-data',
      },
    });
    return res.data;
  } catch (error) {
    if (error.response) {
      helpers.showMessage(
        error.response.data.msg || 'Lỗi hệ thống.Vui lòng thử lại sau',
      );
      console.log(error.response, 'ooooo');
    } else if (error.request) {
      console.log(error.request);
    } else {
      console.log('Error', error.message);
    }
    console.log(error.config);
    // return Promise.reject(error.response);
  }
}

export async function confrimHoa_Don({
  billAmount,
  code,
  receipt,
  receiptImage,
}) {
  // console.log(billAmount)
  // console.log(code)
  // console.log(receipt)
  // console.log(receiptImage)
  try {
    const res = await axios.post(`/${endpoint.CONFIRM_HOADON}`, {
      code,
      billAmount,
      receipt,
      receiptImage,
    });
    return res.data;
  } catch (error) {
    if (error.response) {
      console.log(error.response);
      helpers.showMessage(
        error.response.data.msg || 'Lỗi hệ thống.Vui lòng thử lại sau',
      );
    } else if (error.request) {
      console.log(error.request);
    } else {
      console.log('Error', error.message);
    }
    console.log(error.config);
    // return Promise.reject(error.response);
  }
}

export async function checkOutCode({code}) {
  // console.log(code);
  try {
    const res = await axios.post(`/${endpoint.CHECK_OUT_CODE}`, {
      code,
    });
    return res.data;
  } catch (error) {
    if (error.response) {
      console.log(error.response);
      helpers.showMessage(
        error.response.data.msg || 'Lỗi hệ thống.Vui lòng thử lại sau',
      );
    } else if (error.request) {
      console.log(error.request);
    } else {
      console.log('Error', error.message);
    }
    // console.log(error.config);
    // return Promise.reject(error.response);
  }
}

export async function check_Phone({phone, totalBill, codeBill}) {
  try {
    const res = await axios.post(`/${endpoint.CHECK_PHONE}`, {
      phone,
      bill: totalBill,
      receipt: codeBill,
    });
    return res.data;
  } catch (error) {
    if (error.response) {
      console.log(error.response);
      helpers.showMessage(
        error.response.data.msg || 'Lỗi hệ thống.Vui lòng thử lại sau',
      );
    } else if (error.request) {
      console.log(error.request);
    } else {
      console.log('Error', error.message);
    }
    // console.log(error.config);
    // return Promise.reject(error.response);
  }
}

export async function get_List_Promotion({code}) {
  console.log(code);
  try {
    console.log(code);
    const res = await axios.post(`/${endpoint.GET_LIST_PROMOTION}`, {
      code,
    });

    return res.data;
  } catch (error) {
    console.log(error);
    if (error.response) {
      console.log(error.response);
      helpers.showMessage(
        error.response.data.msg || 'Lỗi hệ thống.Vui lòng thử lại sau',
      );
    } else if (error.request) {
      console.log(error.request);
    } else {
      console.log('Error', error.message);
    }
    // console.log(error.config);
    // return Promise.reject(error.response);
  }
}

export async function getSession() {
  try {
    const res = await axios.get(`/${endpoint.SESSION}`);
    return res.data.result;
  } catch (error) {
    return false;
  }
}

export async function callSignUp(payload) {
  const res = await axios.post(`/${endpoint.SIGN_UP}`, payload);
  // console.log(res.data);
  return res.data;
}

export async function getWorkCalendar({startTime, endTime}) {
  try {
    const start = moment(startTime).format('DD-MM-YYYY');
    const end = moment(endTime).format('DD-MM-YYYY');
    const res = await axios.get(
      `/${endpoint.WORK_CALENDAR}?start_date=${start}&end_date=${end}`,
    );
    return res.data.data;
  } catch (error) {
    console.log(error.response);
    return Promise.resolve([]);
  }
}

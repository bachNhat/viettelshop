import { StyleSheet } from 'react-native'
import { colors } from "../../../constants/theme";
import Layout from '../../../constants/layout';
const styles = StyleSheet.create({
    container: {
        width: Layout.window.width,
        height: Layout.window.height,
        backgroundColor: "rgba(0, 0, 0, 0.6)",
        justifyContent: "center",
        alignItems: "center",
        zIndex: 10
    },
    wrapMessage: {
        minWidth: "75%",
        maxWidth: '80%',
        backgroundColor: "white",
        borderRadius: 5,
        padding: 15,
        paddingBottom: 10,
        justifyContent: 'space-between'
    },
    wrapTitle: {
        flexDirection: "row",
        paddingBottom: 3
    },
    titleMessage: { fontWeight: "bold", fontSize: 20, },
    contentMessage: { fontSize: 16, lineHeight: 20, color: '#555555' },
    wrapButton: { marginTop: 20, alignItems: 'flex-end', },
    buttonMessage: {
        height: 35,
        padding: 8,
        paddingRight: 0,
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        minWidth: 80,

    },
    txtButton: { color: colors.PRIMARY, fontSize: 14, fontWeight: 'bold' }
})

export default styles;
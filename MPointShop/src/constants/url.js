const baseUrl = 'http://prod.mpoint.vn/';
const prefix = 'app/thanhvd';
const endpoint = {
  HISTORYSHOP: 'code/shopGetHistories?page=57&api=shopGetHistories',
  LOGIN: 'user/login',
  EDIT_PASS_URL: 'user/changeUserPassword?page=15&api=userChangePassword',
  CHECK_OTP: 'otp/confirmOTP',
  SEEN_OTP: 'otp/createOTP',
  CHECK_TOKEN_OTP: 'user/mergeShopUser?page=37&api=mergeShopUser',
  UP_IMG: 'image/upload?folder=profile&w=200&h=200',
  CONFIRM_HOADON: 'code/shopCheckoutCode?page=57&api=shopCheckoutCode',
  CHECK_OUT_CODE:
    'partnerproductorder/checkoutOrderCode?page=37&api=checkoutOrderCode',
  CHECK_PHONE: 'code/addPointByPhone?page=57&api=addPointByPhone',
  REFRESH_TOKEN: 'user/refreshToken',
  GET_LIST_PROMOTION:
    'shop/getPromotionByCardCode?page=57&api=getPromotionByCardCode',

  ME: 'auth/mobile/employee',
  WORK_PLACES: 'mobile/workplaces',
  SIGN_UP: 'mobile/register_employee',
  CURRENT_WORK_PLACES: 'mobile/current-workplace',
  STATUS_CHECKIN: 'mobile/checking-status',
  REGISTER: 'cuahang/dangky',
  CHECKIN: 'mobile/checkin',
  CHECKOUT: 'mobile/checkout',
  SESSION: 'histories',
  UPLOAD: 'mobile/upload/checkin',
  WORK_CALENDAR: 'mobile/work-calendars',
  GET_CODE_INFO: 'code/checkCodeInfo?page=57&api=checkCodeInfo',
};

export {baseUrl, endpoint, prefix};

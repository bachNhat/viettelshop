import React, {Component} from 'react';
import {Provider} from 'react-redux';
import configureStore from './src/store';
import {createAppContainer} from 'react-navigation';
import AppNavigator from './src/appNavigator';
import UI from './src/components/UI';
import helpers from './src/globals/helpers';
import {
  setJSExceptionHandler,
  setNativeExceptionHandler,
} from 'react-native-exception-handler';

const store = configureStore();
helpers.setStore(store);
const AppContainer = createAppContainer(AppNavigator);

const errorHandler = (e, isFatal) => {
  alert(e);
};
setJSExceptionHandler(errorHandler, true);

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <AppContainer />
        <UI />
      </Provider>
    );
  }
}

export default App;
